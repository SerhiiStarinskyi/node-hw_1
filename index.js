const express = require('express'),
  app = express(),
  fs = require('fs'),
  morgan = require('morgan'),
  path = require('path');

app.use(express.json());
app.use(morgan('tiny'));

const fileNameTemplate = new RegExp(/^[A-z1-9.]+\.(txt|log|json|xml|js|yaml)$/);

fs.mkdir(path.join(__dirname, 'api/files'), { recursive: true }, (err) => {
  if (err) {
    return console.error(err);
  }
});

app.post('*/api/files', (req, res) => {
  if (!fileNameTemplate.test(req.body.filename)) {
    res.status(400).json({
      message: 'Please enter correct filename'
    });
  } else if (typeof req.body.content === 'undefined') {
    res.status(400).json({
      message: "Please specify 'content' parameter"
    });
  } else {
    fs.writeFile(
      `./api/files/${req.body.filename}`,
      req.body.content,
      (err) => {
        if (err) return console.log(err);
      }
    );
    res.status(200).json({ message: 'File created successfully' });
  }
});

app.get('/api/files', (req, res) => {
  fs.readdir('./api/files', (err, files) => {
    let arr = [];
    if (err) console.log(err);
    else {
      files.forEach((file) => {
        arr.push(file);
      });
    }
    res.status(200).json({
      message: 'Success',
      arr
    });
  });
});

app.get('/api/files/:filename', (req, res) => {
  fs.stat(`./api/files/${req.params.filename}`, (err, stat) => {
    if (err) {
      res.status(400).json({
        message: `No file with '${req.params.filename}' filename found`
      });
    } else {
      const extens = req.params.filename.split('.');

      fs.readFile(`./api/files/${req.params.filename}`, 'utf8', (err, data) => {
        if (err) console.log(err);
        res.status(200).json({
          message: 'Success',
          filename: req.params.filename,
          content: data,
          extension: extens[extens.length - 1],
          uploadedDate: stat.birthtime
        });
      });
    }
  });
});

app.use((err, req, res, next) => {
  res.status(500).send({ message: 'Server error' });
});

app.listen(8080);
